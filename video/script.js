document.addEventListener('DOMContentLoaded', () => { 
  // This is the bare minimum JavaScript. You can opt to pass no arguments to setup.

  var controls = [
    'play',
    'progress',
    'mute',
    'volume',
    'fullscreen',
    'tooltips'
  ];

  var tooltips = {
    controls: false
  }

  var player = new Plyr('#player', { controls });

  // Expose
  window.player = player;

  // Bind event listener
  function on(selector, type, callback) {
    document.querySelector(selector).addEventListener(type, callback, false);
  }
});